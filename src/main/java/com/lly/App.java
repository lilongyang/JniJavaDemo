package com.lly;

public class App
{
    public static void main( String[] args )
    {
        System.out.println(System.getProperty("java.library.path"));
        System.loadLibrary("libJniDemo");
        CountNum countNum = new CountNum();
        int resultInt = countNum.addInt(2, 3);
        System.out.println(">>> int :" + resultInt);

        double resultDouble = countNum.addDouble(3D, 4D);
        System.out.println(">>> double :" + resultDouble);
    }
}
